FROM python:3.8.15-slim
ENV DEBIAN_FRONTEND=noninteractive \
  PYTHONFAULTHANDLER=1 \
  PYTHONUNBUFFERED=1 \
  PYTHONHASHSEED=random \
  PYTHONDONTWRITEBYTECODE=1 \
  PIP_NO_CACHE_DIR=off \
  PIP_DISABLE_PIP_VERSION_CHECK=on \
  PIP_DEFAULT_TIMEOUT=100 \
  APP_DIR=/opt/crud_sistema_big \
  ACCEPT_EULA=Y 
WORKDIR ${APP_DIR}
RUN set -eux; \
  python3 -m venv ${APP_DIR}; \
  ${APP_DIR}/bin/pip install --no-cache-dir --upgrade pip
COPY requirements.txt /tmp
RUN set -eux; \
  savedAptMark="$(apt-mark showmanual)"; \
  apt-get update; \
  apt-get install -y --no-install-recommends \
  gcc \
  g++ \
  libpq-dev \
  ; \
  ${APP_DIR}/bin/pip install -r /tmp/requirements.txt; \
  rm -f /tmp/requirements.txt; \
  apt-mark auto '.*' > /dev/null; \
  apt-mark manual $savedAptMark; \
  apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false; \
  rm -rf /var/lib/apt/lists/*
RUN set -eux; \
  apt-get update; \
  apt-get upgrade -y; \
  apt-get install -y --no-install-recommends \
  libpq-dev \
  ; \
  apt-get clean; \
  rm -rf /var/lib/apt/lists/*
RUN set -eux; \
  addgroup --gid 1000 aescobar; \
  adduser --system --uid 1000 --gid 1000 aescobar
ARG APP_VERSION
COPY dist/crud_sistema_big-${APP_VERSION}.tar.gz /tmp
RUN set -eux; \
  ${APP_DIR}/bin/pip install --no-deps /tmp/crud_sistema_big-${APP_VERSION}.tar.gz; \
  rm /tmp/crud_sistema_big-${APP_VERSION}.tar.gz
USER aescobar
ENV PATH="$APP_DIR/bin:${PATH}"
STOPSIGNAL SIGINT
EXPOSE 8000
CMD ["gunicorn", "--access-logfile=-", "--bind=0.0.0.0:8000", "--worker-tmp-dir=/dev/shm", "crud_sistema_big.wsgi"]
