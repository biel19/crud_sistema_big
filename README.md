# Test: Prueba Backend, Desarrollador Python
Crear una API REST para la administración de Empleados con diferentes controles de accesos:
- Administrador : Contultas, Altas, Bajas y Cambios en Empleados.
- Operador : Solo Lectura de uno o todos los Empleados.
- Supervisor : Consultas y Cambios en Empleados.

## Requerimientos

1. Intalación de docker

        https://drive.google.com/file/d/1KloNhZffMbVpa6hIuldiq5N4HvCjCbd8/view?usp=share_link

2. Intalación de docker-compose
        
        sudo apt install docker-compose

## Instalación

1. Ubicarse en la ruta del proyecto
    
        /path/crud_sistema_big

2. Construir la imagen con docker
        
        docker build --build-arg APP_VERSION=0.1.0 -t crud_sistema_big:0.1.0 .

3. Levantar la imagen con docker-compose

        docker-compose up -d

## Carga de db-dummy

1. Obtener [CONTAINER ID] de **crud_sistema_big:0.1.0**

        docker ps -a

2. Copiar **db-dump.json** al contenedor a la ruta **/opt/crud_sistema_big**

        docker cp db-dump.json CONTAINER ID:/opt/crud_sistema_big

3. Ingresar al bash del contenedor con el [CONTAINER ID]

        docker exec -it -u root CONTAINER ID bash

4. Ya dentro del bash del contenedor ingresar el comando

        django-admin migrate

5. Realizar la carga **db-dump.json** dentro del bash del contenedor

        django-admin loaddata db-dump.json

6. Salir del bash del contenedor

        exit


## Ejemplo

<table class="tg">
<thead>
<tr>
<th class="tg-0lax">Endpoint</th>
<th class="tg-8k7v"><span style="color: #1b2733; background-color: #f7f9fa;"><code class=" prettyprinted" style=""><span class="pln">GET</span></code></span></th>
<th class="tg-8k7v"><span style="color: #1b2733; background-color: #f7f9fa;"><code class=" prettyprinted" style=""><span class="pln">POST</span></code></span></th>
<th class="tg-8k7v"><span style="color: #1b2733; background-color: #f7f9fa;"><code class=" prettyprinted" style=""><span class="pln">PUT</span></code></span></th>
<th class="tg-a18q"><span style="color: #1b2733; background-color: #f7f9fa;"><code class=" prettyprinted" style=""><span class="pln">DELETE</span></code></span></th>
</tr>
</thead>
<tbody>
<tr>
<td class="tg-8k7v"><span style="color: #1b2733; background-color: #f7f9fa;"><code class=" prettyprinted" style=""><span class="pln">api</span><span class="pun">/</span><span class="pln">empleado</span><span class="pun"></span></code></span></td>
<td class="tg-0lax">1. Lista todos los empleados</td>
<td class="tg-0lax">2. Crea un nuevo empleador</td>
<td class="tg-0lax">N/A</td>
<td class="tg-0lax">N/A</td>
</tr>
<tr>
<td class="tg-8k7v"><span style="color: #1b2733; background-color: #f7f9fa;"><code class=" prettyprinted" style=""><span class="pln">api</span><span class="pun">/</span><span class="pln">empleado</span><span class="pun">/&lt;</span><span class="kwd">int</span><span class="pun">:</span><span class="pln">id</span><span class="pun">&gt;</span></code></span></td>
<td class="tg-0lax">3. Retrieve: Obtiene al empleado que se esta consultando  <span style="color: #1b2733; background-color: #f7f9fa;"><code class=" prettyprinted" style=""><span class="pln">id</span></code></span></td>
<td class="tg-0lax">N/A</td>
<td class="tg-0lax">4. Update: Actualiza la información del empleado <span style="color: #1b2733; background-color: #f7f9fa;"><code class=" prettyprinted" style=""><span class="pln">id</span></code></span></td>
<td class="tg-0lax">5. Delete: Elimina al empleado <span style="color: #1b2733; background-color: #f7f9fa;"><code class=" prettyprinted" style=""><span class="pln">id</span></code></span></td>
</tr>
</tbody>
</table>

1. Para poder realizar el consumo del servicio es necesario una autenticación básica (Basic Auth), cada usuario cuenta con sus permisos correspondientes

        - Administrador
            Username: user_admin
            Password: userAdmin123
        - Operador
            Username: user_operador
            Password: userOperador123
        - Supervisor
            Username: user_supervisor   
            Password: userSupervisor123

2. Prueba realizada con curl con el usuario **Administrador**


        curl --location --request GET 'http://127.0.0.1:8000/api/empleado/3' \
        --header 'Authorization: Basic dXNlcl9hZG1pbjp1c2VyQWRtaW4xMjM=' \
        --header 'Content-Type: application/json' \
        --data-raw '{
            "nombre": "LIZBETH JAUREGUI CALVILLO",
            "curp": "JACL841028MJCRLZ00",
            "rfc": "JACL841028Q62 ",
            "cp": 1522,
        "fecha_nacimento": "1990-01-12"
        }'


