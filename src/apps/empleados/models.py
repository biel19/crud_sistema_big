from django.db import models

# Create your models here.


class Empleados(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=100,  null=True,  blank=True)
    curp = models.CharField(max_length=18,  null=True,  blank=True)
    rfc = models.CharField(max_length=13,  null=True,  blank=True)
    cp = models.IntegerField(null=True,  blank=True)
    fecha_nacimento = models.DateTimeField(null=True,  blank=True)

    class Meta:
        app_label = 'empleados'
        db_table = 'sistema_big_empleados '
