from django.urls import path
from .views import (
    EmpleadosListApiView,
    EmpleadosApiView
)

urlpatterns = [
    path('empleado', EmpleadosListApiView.as_view()),
    path('empleado/<int:id>', EmpleadosApiView.as_view()),
]
