# Identificador de resultado de error
UPDATE = 0
UPDATED_PREVIOUSLY = 1
NOT_REGISTERED = 2
DATA_TYPE_ERROR = 3
LENGHT_ERROR = 4
DATA_VALUE_ERROR = 5
OBLIGATORY_ERROR = 6
SQL_ERROR = 7
CONNECTION_ERROR = 8
INTERNAL_ERROR = 9
OTHER_ERROR = 10
STRUCTURE_ERROR = 11

# ;Mensaje de error
SUCCESFULL_MESSAGE = ''
UPDATE_MESSAGE = ''
UPDATED_PREVIOUSLY_MESSAGE = ''
DATA_TYPE_ERROR_MESSAGE = 'Error por tipo de dato'
LENGHT_ERROR_MESSAGE = 'Error por longitud de dato'
DATA_VALUE_ERROR_MESSAGE = 'Error por valor'
OBLIGATORY_ERROR_MESSAGE = 'Error por obligatoriedad'
SQL_ERROR_MESSAGE = 'Error SQL'
CONNECTION_ERROR_MESSAGE = 'Error de conexión'
INTERNAL_ERROR_MESSAGE = 'Otro error'
STRUCTURE_ERROR_MESSAGE = 'Error de estructura JSON'

# Validación de los campos que se mandan en la solicitud
TIPO_VALIDACION = {
    'nombre': 'combinacion_1',
    'curp': 'combinacion_2',
    'rfc': 'combinacion_3',
    'cp': 'combinacion_4',
    'fecha_nacimento': 'combinacion_5'
}
# Configuración de las validaciones de los campos que
# se envían en la solicitud.
VALIDACIONES = {
    'combinacion_1': {'tamanio_campo': 100, 'tipo_dato': None,  'requerido': True},
    'combinacion_2': {'tamanio_campo': 18, 'tipo_dato': None,  'requerido': True},
    'combinacion_3': {'tamanio_campo': 13, 'tipo_dato': None,  'requerido': True},
    'combinacion_4': {'tamanio_campo': None, 'tipo_dato': 'isdigit',  'requerido': True},
    'combinacion_5': {'tamanio_campo': None, 'tipo_dato': 'isdate',  'requerido': True},
}


class CheckValues:
    def __init__(self, parameters, type_validation, key_name, model=None):
        self.parameters = parameters
        self.model = None
        self.data_type = None
        self.required = None
        self.tamanio_campo = None
        self.type_validation = type_validation
        self.is_check_value = {
            'isalnum': self.isalnum_check,
            'isdigit': self.isdigit_check,
            'islist': self.islist_check,
            'isdate': self.isdate_check
        }
        self.key_name = key_name

    def response(self):
        if (not self.type_validation):
            error = {
                'error': 'Error de estructura JSON',
                'success': False}
            return False, STRUCTURE_ERROR, error

        validations = VALIDACIONES.get(self.type_validation)
        function_type_data = self.is_check_value.get(validations['data_type'])
        valid_required = validations['requerido']
        valid_lenght = validations['tamanio_campo']
        error = {}

        if (valid_required and not self.required_check(self.parameters)):
            error = {'field': self.key_name,
                     'error': self.parameters,
                     'message':  OBLIGATORY_ERROR_MESSAGE,
                     'Result': OBLIGATORY_ERROR}
            return False, OBLIGATORY_ERROR, error

        if (valid_lenght and not self.lenght_check(self.parameters, valid_lenght)):
            error = {'field': self.key_name,
                     'error': self.parameters,
                     'message':  LENGHT_ERROR_MESSAGE,
                     'Result': LENGHT_ERROR}
            return False, LENGHT_ERROR, error

        if (function_type_data and not function_type_data(self.parameters)):
            error = {
                'error': 'Error de estructura JSON',
                'success': False}
            return False, STRUCTURE_ERROR, error

        return True, UPDATE, error

    def isalnum_check(self, value):
        try:
            return value.isalnum()
        except Exception:
            return False

    def isdigit_check(slef, value):
        try:
            return str(value).isdigit()
        except Exception:
            return False

    def required_check(slef, value):
        if (value not in [None, '', u'']):
            return True
        return False

    def lenght_check(self, value, lengh_value):
        return len(str(value)) <= lengh_value

    def compare_check(self, value, compare):
        if (value in compare):
            return True
        return False

    def islist_check(self, value):
        if (isinstance(value, list)):
            return True
        return False

    def isdate_check(self, value):
        return True
