from rest_framework import serializers
from apps.empleados.models import (
    Empleados
)
from rest_framework.validators import UniqueTogetherValidator
from rest_framework.serializers import Serializer, FileField


class EmpleadosSerializer(serializers.ModelSerializer):
    class Meta:
        model = Empleados
        fields = ['id', 'nombre', 'curp', 'rfc', 'cp', 'fecha_nacimento']
        validators = [
            UniqueTogetherValidator(
                queryset=Empleados.objects.all(),
                fields=['nombre']
            )
        ]
