from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework import permissions
from apps.empleados.models import (
    Empleados
)
from .serializers import (
    EmpleadosSerializer
)
from rest_framework.permissions import DjangoModelPermissions
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
# Create your views here.


class EmpleadosListApiView(APIView):
    permission_classes = [DjangoModelPermissions]
    queryset = Empleados.objects.all()

    def get(self, request, *args, **kwargs):
        obj_crud = CRUDBase(Empleados, EmpleadosSerializer, {})
        serializer = obj_crud.get_multi()
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):

        data = {
            'nombre': request.data.get('nombre'),
            'curp': request.data.get('curp'),
            'rfc': request.data.get('rfc'),
            'cp': request.data.get('cp'),
            'fecha_nacimento': request.data.get('fecha_nacimento')
        }
        obj_crud = CRUDBase(Empleados, EmpleadosSerializer, data)
        serializer = obj_crud.create()
        return serializer


class EmpleadosApiView(APIView):
    permission_classes = [DjangoModelPermissions]
    queryset = Empleados.objects.all()

    def get_object(self, id):
        try:
            obj_crud = CRUDBase(Empleados, None, None)
            obj_crud.id = id
            return obj_crud.get()
        except Empleados.DoesNotExist:
            return None

    def get(self, request, id, *args, **kwargs):
        resultado = self.get_object(id)
        if (not resultado):
            return Response(
                {'Objeto con el id no existe'},
                status=status.HTTP_400_BAD_REQUEST
            )
        obj_crud = CRUDBase(Empleados, EmpleadosSerializer, resultado)
        serializer = obj_crud.get_id()
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, id, *args, **kwargs):
        resultado = self.get_object(id)
        if (not resultado):
            return Response(
                {'result': 'Objeto con el id no existe'},
                status=status.HTTP_400_BAD_REQUEST
            )
        data = {
            'nombre': request.data.get('nombre'),
            'curp': request.data.get('curp'),
            'rfc': request.data.get('rfc'),
            'cp': request.data.get('cp'),
            'fecha_nacimento': request.data.get('fecha_nacimento')
        }
        ob_crud = CRUDBase(Empleados, EmpleadosSerializer, data)
        ob_crud.data_result = resultado
        serializer = ob_crud.put_id()
        return serializer

    def delete(self, request, id, *args, **kwargs):
        resultado = self.get_object(id)
        if (not resultado):
            return Response(
                {'result': 'Objeto con el id no existe'},
                status=status.HTTP_400_BAD_REQUEST
            )
        obj_crud = CRUDBase(Empleados, EmpleadosSerializer, {})
        obj_crud.data_result = resultado
        return obj_crud.delete_id()


class CRUDBase():
    def __init__(self, model, serializer, data):
        self.model = model
        self.serializer = serializer
        self.data = data
        self.id = None
        self.data_result = None

    def get(self):
        return self.model.objects.get(id=self.id)

    def get_id(self):
        serializer = self.serializer(self.data)
        return serializer

    def create(self):
        serializer = self.serializer(data=self.data)
        if (serializer.is_valid()):
            serializer.save()
            return Response({"result": "Objeto fue creado!!"}, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_multi(self):
        todos = self.model.objects.all()
        serializer = self.serializer(todos, many=True)
        return serializer

    def put_id(self):
        serializer = self.serializer(
            instance=self.data_result, data=self.data, partial=True)
        if (serializer.is_valid()):
            serializer.save()
            return Response({"result": "Objeto actualizado!!"}, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete_id(self):
        self.data_result.delete()
        return Response(
            {"result": "Objeto eliminado!!"},
            status=status.HTTP_200_OK
        )
